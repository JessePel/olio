/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jesse
 */
public class Character {
 
    private final String[] characters;
    private final String[] weapons;
    private String hahmo;
    private String ase;
    
    public Character() {
        this.characters = new String[] {"King", "Knight", "Queen", "Troll"};
        
        this.weapons = new String[] {"Knife", "Axe", "Sword", "Club"};
    }
    
    public void Characters() {
        System.out.println("Valitse hahmosi: ");
        System.out.println("1) Kuningas");
        System.out.println("2) Ritari");
        System.out.println("3) Kuningatar");
        System.out.println("4) Peikko");
    }
    public void createCharacter(int val1, int val2) {
        this.hahmo = this.characters[val1-1];
        this.ase = this.weapons[val2-1];
    }
    public void weapon() {
        System.out.println("Valitse aseesi: ");
        System.out.println("1) Veitsi");
        System.out.println("2) Kirves");
        System.out.println("3) Miekka");
        System.out.println("4) Nuija");
    }
    
    public void Fight() {
        System.out.println(this.hahmo + " tappelee aseella " + this.ase);
    }

}
