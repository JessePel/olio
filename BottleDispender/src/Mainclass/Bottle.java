package Mainclass;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jesse
 */
public class Bottle {

    private String manufacturer;
    private String name;
    private double total_energy;
    private double price;
    private double size;

    public Bottle(String nimi, String manuf, double totE) {
        name = nimi;
        manufacturer = manuf;
        total_energy = totE;
        price = 1.8;
        size = 0.5;

    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public double getEnergy() {
        return total_energy;
    }

    public double getPrice() {
        return price;
    }

    public double getSize() {
        return size;
    }
}
