/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mainclass;

/**
 *
 * @author jesse
 */
public class BottleDispenser {
    private int pullo;
    private int raha;
    private Bottle[] bottle_arrays;
    Bottle pull = new Bottle("Pepsi Max", "Pepsi", 0.3);
    
    public BottleDispenser(){
        pullo = 6;
        raha = 0;
        
        bottle_arrays = new Bottle[pullo];
        
        for(int i = 0;i< pullo; i++) {
            bottle_arrays[i] = new Bottle("Pepsi Max", "Pepsi", 0.3);
        }
    }
    
    public void lisaaRahaa() {
        raha += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    public void ostaPullo() {
        if (raha < pull.getPrice()) {
            System.out.println("Syötä rahaa ensin!");
            
	} else if (pullo > 0 && raha > pull.getPrice()) {
            pullo -= 1;
            raha -= 1;
            System.out.println("KACHUNK! " + pull.getName() + " tipahti masiinasta!");
        }
        
    }
    public void palautaRaha() {
        raha = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
    public void tulosta() {
        for (int i = 1; i <= pullo; i++) {
            System.out.println(i + ". Nimi: " + pull.getName() + "\n\t" + "Koko: " + pull.getSize() + "\tHinta: " + pull.getPrice());
        }
    }
}