import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jesse
 */

public class Car {

    private ArrayList<String> lista = new ArrayList<String>();

    class Part {

        private String osa;
        private int maara;

        public String getOsa() {
            return this.osa;
        }

        public void setOsa(String tyyppi) {
            this.osa = tyyppi;
        }

        public void setMaara(int maara) {
            this.maara = maara;
        }

    }

    public void luoAuto() {
        body();
        chassis();
        engine();
        wheel();

    }

    public void wheel() {
        Part wheel = new Part();
        int amount = 0, i;
        for (i = 0; i < 4; i++) {
            System.out.println("Valmistetaan: Wheel");
            amount++;
        }
        lista.add("4 Wheel");

    }

    public void body() {
        System.out.println("Valmistetaan: Body");
        lista.add("Body");
    }

    public void chassis() {
        System.out.println("Valmistetaan: Chassis");
        lista.add("Chassis");
    }

    public void engine() {
        System.out.println("Valmistetaan: Engine");
        lista.add("Engine");
    }

    public void print() {
		System.out.println("Autoon kuuluu:");
        for (int i = 0; i < lista.size(); i++) {

            System.out.println("\t" + lista.get(i));
        }

    }
}
