/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

import java.util.Scanner;

/**
 *
 * @author Jesse Peltola
 * @2.4
 */
public class Mainclass {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Anna koiralle nimi: ");
        String joku = scan.nextLine();
        Dog dog1 = new Dog(joku);

        System.out.print("Mitä koira sanoo: ");
        String say = scan.nextLine();

        dog1.speak(say);

    }

}
