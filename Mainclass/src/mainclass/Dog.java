/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;
import java.util.Scanner;
/**
 *
 * @author Jesse Peltola
 */
public class Dog {

    private String name;
    private final String says = "Much wow!";

    public Dog(String n) {
        if (n.trim().isEmpty()) {
            name = "Doge";
        } else {
            System.out.println("Hei, nimeni on " + n);
            name = n;
        }
    }
    public void speak(String word) {
        if (word.trim().isEmpty()) {
            System.out.println(name + ": " + says);
        } else {
            System.out.println(name + ": " + word);
        }
        Scanner scan = new Scanner(word);
        
        while (scan.hasNext()){
            if(scan.hasNextBoolean()) {
                System.out.println("Such boolean: " + scan.nextBoolean());
            } else if(scan.hasNextInt()) {
                System.out.println("Such integer: " + scan.nextInt());
            } else {
                System.out.println(scan.nextLine());
            }
            
        }
        }
}
