/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mainclass;

/**
 *
 * @author jesse
 */
import java.util.Scanner;
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        BottleDispenser pullo = new BottleDispenser();

        while (true) {
            System.out.println("\n*** LIMSA-AUTOMAATTI ***\n"
                    + "1) Lisää rahaa koneeseen\n"
                    + "2) Osta pullo\n"
                    + "3) Ota rahat ulos\n"
                    + "4) Listaa koneessa olevat pullot\n"
                    + "0) Lopeta");
            System.out.print("Valintasi: ");
            int val = scan.nextInt();
            if (val == 0) {
                break;
            }
            switch (val) {
                case 1:
                    pullo.lisaaRahaa();
                    break;
                case 2:
                    pullo.ostaPullo();
                    break;
                case 3:
                    pullo.palautaRaha();
                    break;
                case 4:
                    pullo.tulosta();
                default:
                    break;

            }

        }

    }
}