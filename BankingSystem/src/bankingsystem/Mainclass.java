/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankingsystem;

import java.util.Scanner;

/**
 *
 * @author Jesse Peltola
 */
public class Mainclass {

    public static void main(String[] args) {
        int valinta = -1;
        String tilinumero;
        int maara;
        int luottoraja;
        Scanner scan = new Scanner(System.in);
        while (valinta != 0) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            valinta = Integer.parseInt(scan.nextLine());
            switch (valinta) {

                case 1:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.nextLine();

                    System.out.print("Syötä rahamäärä: ");
                    maara = Integer.parseInt(scan.nextLine());
                    
                    System.out.println("Tilinumero: " + tilinumero + "\n" + "Rahamäärä: " + maara);
                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.nextLine();
                    System.out.print("Syötä rahamäärä: ");
                    maara = Integer.parseInt(scan.nextLine());
                    
                    System.out.print("Syötä luottoraja: ");
                    luottoraja = Integer.parseInt(scan.nextLine());
                    
                    System.out.println("Tilinumero: " + tilinumero + "\n" + "Rahamäärä: " + maara + "\n" + "Luotto: " + luottoraja);
                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.nextLine();

                    System.out.print("Syötä rahamäärä: ");
                    maara = Integer.parseInt(scan.nextLine());
                    System.out.println("Tilinumero: " + tilinumero + "\n" + "Rahamäärä: " + maara);
                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.nextLine();

                    System.out.print("Syötä rahamäärä: ");
                    maara = Integer.parseInt(scan.nextLine());
                    System.out.println("Tilinumero: " + tilinumero + "\n" + "Rahamäärä: " + maara);
                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");

                    tilinumero = scan.nextLine();
                    System.out.println("Tilinumero: " + tilinumero);
                 
                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    tilinumero = scan.nextLine();
                    System.out.println("Tilinumero: " + tilinumero);
                    break;
                case 7:
                    System.out.println("Tulostaa kaiken");
                    break;
                case 0:
                    valinta = 0;
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
                    break;
            }
        }

        // TODO code application logic here
    }

}
