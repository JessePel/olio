import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jesse
 *
 */
class IOfile {

    public void readAndWrite(String r, String w ) {
        String output = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(r));
            BufferedWriter wr = new BufferedWriter(new FileWriter(w));
            
            while ((output = br.readLine()) != null) {
                if (output.trim().length() > 0 && output.trim().length() < 30 && output.contains("v")) {
                    
                    wr.write(output + "\n");
                }
               

            }

            br.close();
            wr.close();
        } catch (IOException ex) {
            System.out.println("Error");
        }
    }
}
